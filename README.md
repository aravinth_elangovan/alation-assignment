# Load Balancing

**OS used:**
	

 - Host (for Virtual machines) -> Windows 10
 - Virtual Machine -> Cent OS 7 minimal

> Note: For better understanding I have set the host entries in my local windows machine as follows,

**File :** C:\Windows\System32\drivers\etc\hosts

	192.168.52.63 load-balancer
	192.168.52.14 webserver-1
	192.168.52.9 webserver-2
	


# Find the following completion steps on the given assignment

**Step 1:** Created the 3 virtual machines A, B and C.

	A - Load Balancer  --> ip: 192.168.52.63,
	B - Webserver-1  --> ip: 192.168.52.14,
	C - Webserver-2 --> ip: 192.168.52.9.


**Step 2:** Added the host names in all three virtual servers.
	**Path:** /etc/hosts 

	192.168.52.63 load-balancer
	192.168.52.14 webserver-1
	192.168.52.9 webserver-2

**Step 3:** Installed apache in all three servers by the following commands,

	# To install apache (httpd)
	yum install httpd -y
	
	# To start apache
	service httpd start
	
	# To enable the apache service by machine boot
	systemctl enable httpd

**Step 4:** Deployed the firstpage.html file in both webservers (B and C). The firstpage.html file contains the following items,

**Path:** /var/www/html

	webserver-1  --> firstpage.html has the string "Hello World from webserver 1"
	webserver-2  --> firstpage.html has the string "Hello World from webserver 2"

**Step 5:** Deployed loadbalancer.conf file in loadbalancer server (A).

**Path:** /etc/httpd/conf.d/

The loadbalancer.conf file contains the following :
	
	
		<Proxy balancer://load-balancer>
			BalancerMember http://192.168.52.14:80
			BalancerMember http://192.168.52.9:80
			ProxySet lbmethod=byrequests
		</Proxy>
		ProxyPass / balancer://load-balancer/

For Round-Robin algorithm, I have used this line "ProxySet lbmethod=byrequests" in loadbalancer configuration file.

After adding this loadbalancer.conf file, restart the apache,

	service httpd restart

Now we are all set and ready to go. Hit the load-balancer with "/firstpage.html" path i.e., http://load-balancer/firstpage.html we can get the response from webserver-1 as "Hello World from webserver 1". If you hit another time we can get the response from webserver-2 as "Hello World from webserver 2" for this we have used the Round-Robin algorithm in load balancer.

**Step 6:** Now I'm removing the webserver-2 from the load-balancer i.e., removing this line "BalancerMember http://192.168.52.9:80" from loadbalancer.conf file and restarted the apache in load-balancer server.

**Step 7:** Now hit the load balancer we can get the response from webserver-1 without any failures.

FYI, If any one of the webserver apache is down it sends the request to the active apache webserver.

Please find the result of **Step 5** and **Step 7** in the attached video clips.

## Summary for this solution:

1. We can reduce the downtime in deployment or any changes in server by this solution.
2. By this we can manage the failures without affecting the requests (hits).
3. It increases the scalability of the requests.
## Disliked about this solution:
1. The load balancer requires the another separate server, which tends to additional cost.
